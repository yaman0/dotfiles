" install vim-plug if not already installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" to fix cursor issue for windows terminal
" fix from https://stackoverflow.com/questions/67110568/showing-block-cursor-in-insert-mode-for-vim-only-works-after-opening-vim-twice
if $TERM =~ "xterm-256color"
    let &t_SI = "\<Esc>[6 q"
    let &t_SR = "\<Esc>[4 q"
    let &t_EI = "\<Esc>[2 q"
endif

" plugin installation : add plugins...
call plug#begin('~/.vim/plugged')
" styling
Plug 'morhetz/gruvbox' 
Plug 'vim-airline/vim-airline'

" file flying
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" tips => type PlugInstall to install plugin
call plug#end()

colorscheme gruvbox
let g:airline_theme='gruvbox'

set nu
set noerrorbells
set background=dark

noremap <SPACE> <Nop>
let mapleader=" "

map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

nnoremap <c-p> <esc>:Files<CR>
