# Dotfiles

My personnal dotfiles and setup for dev on mac, linux or windows.

## Setup 
```bash
./setup
```

## Clean environment
```bash
./clean
````

## Requirements
- [stow](https://www.gnu.org/software/stow/)
- zsh :
  - zsh
  - [antigen](https://github.com/zsh-users/antigen)
- nvim :
  - [neovim (nightly)](https://neovim.io/)
  - for lsp server configuration follow instructions [here](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md) 

