" install vim-plug if not already installed

" to fix cursor issue for windows terminal
" fix from https://stackoverflow.com/questions/67110568/showing-block-cursor-in-insert-mode-for-vim-only-works-after-opening-vim-twice
if $TERM =~ "xterm-256color"
    let &t_SI = "\<Esc>[6 q"
    let &t_SR = "\<Esc>[4 q"
    let &t_EI = "\<Esc>[2 q"
endif

set nu rnu
set noerrorbells
set ai "Auto indent
set si "Smart indent
set nowrap "No Wrap lines
set path+=**

set expandtab
set tabstop=4
set softtabstop=4

set scrolloff=7

noremap <SPACE> <Nop>
let mapleader=" "

map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

nnoremap <leader>p <c-^>
nnoremap <leader>pv :Ex<CR>

syntax enable
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

runtime ./plug.vim

let g:gruvbox_contrast_dark = 'hard'
if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
let g:gruvbox_invert_selection='0'
colorscheme gruvbox
set background=dark


lua require('yamano')


