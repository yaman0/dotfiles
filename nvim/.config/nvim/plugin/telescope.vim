nnoremap <C-p> :lua require('telescope.builtin').git_files()<CR>
nnoremap <leader>grp :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>

