let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

if has("nvim")
	let g:plug_home = stdpath('data') . '/plugged'
endif

call plug#begin()
	Plug 'morhetz/gruvbox'

	Plug 'tpope/vim-fugitive'
	Plug 'tpope/vim-rhubarb'
	Plug 'cohama/lexima.vim'

	if has("nvim")
		Plug 'neovim/nvim-lspconfig'
		Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

        " Completion
        Plug 'hrsh7th/cmp-nvim-lsp'
        Plug 'hrsh7th/cmp-buffer'
        Plug 'hrsh7th/cmp-path'
        Plug 'hrsh7th/cmp-cmdline'
        Plug 'hrsh7th/nvim-cmp'
        Plug 'onsails/lspkind-nvim'
        Plug 'L3MON4D3/LuaSnip'


        " moving
        Plug 'nvim-lua/popup.nvim'
        Plug 'nvim-lua/plenary.nvim'
        Plug 'nvim-telescope/telescope.nvim'
        Plug 'nvim-telescope/telescope-fzy-native.nvim'

        Plug 'hoob3rt/lualine.nvim'

        " web
        Plug 'tpope/vim-surround'
	endif

call plug#end()
